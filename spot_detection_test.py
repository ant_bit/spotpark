import cv2
import numpy as np

# initialize the list of reference point
ref_point = {} # the dict contains all the patches segmented from the origianal image
coordinates = [] # coordinates of a current chosen square
counter = 0 # is needed to form a dict of parking spots

def shape_selection(event, x, y, flags, image):

    # grab references to the global variables
    global ref_point, coordinates, counter

    # if the left mouse button was clicked, record the starting
    # (x, y) coordinates and indicate that cropping is being performed
    if event == cv2.EVENT_LBUTTONDOWN:
        coordinates = [(x, y)]

    # check to see if the left mouse button was released
    elif event == cv2.EVENT_LBUTTONUP:
        # record the ending (x, y) coordinates and indicate that
        # the cropping operation is finished, draws a square not rectangle
        if (x - coordinates[0][0]) > (y - coordinates[0][1]):
            y = coordinates[0][1] + (x - coordinates[0][0])
        else:
            x = coordinates[0][0] + (y - coordinates[0][1])
        coordinates.append((x, y))
        ref_point[counter] = coordinates
        

        # draw a square around the region of interest
        cv2.rectangle(image, ref_point[counter][0], ref_point[counter][1], (0, 255, 0), 2)
        cv2.imshow("image", image)
        counter += 1

def patch_creation():
    "Creates a dict of patches drawn manually"

    # define a video capture object 
    vid = cv2.VideoCapture(0) 

    # capture one frame to locate patches 
    ret, img = vid.read()

    # load the image, clone it, and setup the mouse callback function
    cv2.imshow("image", img)
    clone = img.copy() # clone is needed to realize reset functionality "r"
    cv2.namedWindow("image")
    cv2.setMouseCallback("image", shape_selection, img)

    cv2.imshow("image", img)

    # keep looping until the 'q' key is pressed
    while True:
        # display the image and wait for a keypress
        
        key = cv2.waitKey(1) & 0xFF

        # press 'r' to reset the window
        if key == ord("r"):
            img = clone.copy()
            cv2.imshow("image", img)

        # if the 'c' key is pressed, break from the loop
        elif key == ord("c"):
            cv2.imwrite('labeled.jpg', img)
            break

    print(ref_point) # prints a dict (lable and corresponding coordinates) of drawn patches

    vid.release() 
    # close all open windows
    cv2.destroyAllWindows() 

def spot_detection():
    "Runs live camera imaging and detects if the chosen spots are free or occupied"
    
    # define a video capture object 
    vid = cv2.VideoCapture(0) 
    
    while(True): 
        
        # Capture the video frame by frame
        ret, frame = vid.read()

        # paths to the files needed to inference the model (.prototxt and .caffemodel)
        net = cv2.dnn.readNetFromCaffe('./models/mAlexNet-on-Combined_CNRParkAB_Ext_train-val-PKLot_val/deploy.prototxt',
                                       './models/mAlexNet-on-Combined_CNRParkAB_Ext_train-val-PKLot_val/snapshot_iter_2761.caffemodel')
        
        for label in ref_point:
            
            # crop the whole image to the size of the patch
            patch = frame[ref_point[label][0][1]:ref_point[label][1][1], ref_point[label][0][0]:ref_point[label][1][0]]

            # patch preprocessing (needed to pass it to the model)
            blob = cv2.dnn.blobFromImage(patch, 1, (224, 224), (104, 117, 123))

            # model inference
            net.setInput(blob)
            preds = net.forward()

            # predictions: probabilities for [0 - free, 1 - occupied]
            print(preds)

            # if probability of free parking spot > probability of occupied parking spot
            if preds[0][0] > preds[0][1]:
                # free parking spot
                cv2.rectangle(frame, ref_point[label][0], ref_point[label][1], (0, 255, 0), 2)
            else:
                # occupied parking spot
                cv2.rectangle(frame, ref_point[label][0], ref_point[label][1], (0, 0, 255), 2)
            
        cv2.imshow('frame', frame)

        # press "q" to quit and close the window
        if cv2.waitKey(1) & 0xFF == ord('q'): 
            break
               
    # After the loop release the cap object 
    vid.release() 
    # Destroy all the windows 
    cv2.destroyAllWindows()

def main():

    patch_creation()
    spot_detection()

if __name__ == "__main__":

    main()
    

     